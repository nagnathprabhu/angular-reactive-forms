import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  rForm: FormGroup;
  name: string= '';
  description:string= '';
  post: any;
  validate:any;
  titleAlert:string = "Name is required"

  ngOnInit(){
    this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {
        console.log(validate)
          if (validate == '1') {
              this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
              this.titleAlert = 'You need to specify at least 3 characters';
              this.rForm.controls['name']
          } else {
              this.rForm.get('name').setValidators(Validators.required);
          }
          this.rForm.get('name').updateValueAndValidity();

      });
  }

  constructor(private fb: FormBuilder){
    //modeling the form
    this.rForm = fb.group({
      name: [null, Validators.required],
      description: [null, Validators.compose([Validators.minLength(10), Validators.maxLength(20)])],
      validate:''
    });

  }

  addPost(post){
    this.name= post.name;
    this.description = post.description;
    console.log(post)
  }
}
